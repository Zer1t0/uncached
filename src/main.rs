use clap::Parser;
mod readin;
use chrono::{self, DateTime, Duration, Utc};
use std::collections::HashMap;
use std::fs;
use std::io::ErrorKind;
use toml;

/// Only print new lines, or those which weren't seen in a while
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Targets to process.
    /// If file is passed, its lines will be read.
    /// If none is passed, then stdin will be used.
    #[arg()]
    targets: Vec<String>,

    /// Min seconds to allow values to pass. It accumulates with other time measures.
    #[arg(short, long)]
    seconds: Option<u32>,

    /// Min minutes to allow values to pass. It accumulates with other time measures.
    #[arg(short, long)]
    minutes: Option<u32>,

    /// Min hours to allow values to pass. It accumulates with other time measures.
    #[arg(short = 'H', long)]
    hours: Option<u32>,

    /// Min days to allow values to pass. It accumulates with other time measures.
    #[arg(short, long)]
    days: Option<u32>,

    /// Cache file
    #[arg(short, long)]
    cache: Option<String>,

    /// Do no update the cache file
    #[arg(short, long)]
    no_update: bool,
}

fn main() {
    let args = Args::parse();

    if let Err(err) = main2(args) {
        eprint!("Error: {}", err);
    }
}

fn main2(args: Args) -> Result<(), String> {
    let min_duration =
        gen_min_duration(args.seconds, args.minutes, args.hours, args.days);

    let mut cache = load_cache(&args.cache)?;

    let now = chrono::offset::Utc::now();

    for t in readin::read_inputs(args.targets) {
        if let Some(last_pass) = cache.get(&t) {
            let delta = now - last_pass;
            if delta < min_duration {
                continue;
            }
        }

        println!("{}", t);
        cache.insert(t, now);
    }

    let update = !args.no_update;
    if update {
        if let Some(cache_path) = &args.cache {
            save_cache(cache_path, &cache)?;
        }
    }

    return Ok(());
}

fn save_cache(
    cache_path: &str,
    cache: &HashMap<String, DateTime<Utc>>,
) -> Result<(), String> {
    fs::write(cache_path, toml::to_string(cache).unwrap())
        .map_err(|e| e.to_string())?;
    return Ok(());
}

fn gen_min_duration(
    seconds: Option<u32>,
    minutes: Option<u32>,
    hours: Option<u32>,
    days: Option<u32>,
) -> Duration {
    let mut min_duration = Duration::seconds(0);
    let mut none_specified = true;

    if let Some(seconds) = seconds {
        min_duration = min_duration + Duration::seconds(seconds.into());
        none_specified = false;
    }

    if let Some(minutes) = minutes {
        min_duration = min_duration + Duration::minutes(minutes.into());
        none_specified = false;
    }

    if let Some(hours) = hours {
        min_duration = min_duration + Duration::hours(hours.into());
        none_specified = false;
    }

    if let Some(days) = days {
        min_duration = min_duration + Duration::days(days.into());
        none_specified = false;
    }

    if none_specified {
        min_duration = Duration::milliseconds(i64::MAX);
    }

    return min_duration;
}

fn load_cache(
    cache_path: &Option<String>,
) -> Result<HashMap<String, DateTime<Utc>>, String> {
    if let Some(cache_path) = cache_path {
        match fs::read_to_string(cache_path) {
            Ok(content) => {
                return Ok(toml::from_str(&content)
                    .map_err(|e| format!("Error parsing TOML db: {}", e))?);
            }
            Err(err) => match err.kind() {
                ErrorKind::NotFound => {
                    eprintln!(
                        "Warning: Cache file ({}) not found, using an empty cache",
                        cache_path
                    );
                }
                _ => Err(format!("Error opening DB file: {}", err))?,
            },
        };
    }

    return Ok(HashMap::new());
}
