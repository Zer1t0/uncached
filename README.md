# uncached

Only print new lines, or those which weren't seen in a while

## Usage

Without arguments, uncached will erase repeated lines:
```
$ cat /tmp/inputs.txt
one
two
one
three
uncached$ cat /tmp/inputs.txt | uncached
one
two
three
```

A cache file can be provided to avoid repeat lines in a period of time:
```
$ cat /tmp/inputs.txt
one
two
three
$ # new values are printed
$ cat /tmp/inputs.txt | uncached -c /tmp/in.cache
one
two
three
$ # values are not printed
$ cat /tmp/inputs.txt | uncached -c /tmp/in.cache
$ sleep 5
$ # values older than 4 seconds are printed
$ cat /tmp/inputs.txt | uncached -c /tmp/in.cache
one
two
three
$ echo four >> /tmp/inputs.txt
$ # new values are printed
$ cat /tmp/inputs.txt | uncached -c /tmp/in.cache
four
```
